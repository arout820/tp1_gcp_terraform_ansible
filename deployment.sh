#!/bin/bash

# Chemins
root_dir="$HOME/workplace/tp1-gcp-terraform"
terraform_dir="$root_dir/terraform/configuration"
ansible_dir="$root_dir/ansible"

# Fonction pour installer Terraform s'il est absent
install_terraform() {
    echo "Terraform n'est pas installé. Installation en cours..."
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list >/dev/null
    sudo apt update
    sudo apt install terraform
    if [ $? -eq 0 ]; then
        echo -e "\033[32mTerraform a été installé avec succès.\033[0m"
    else
        echo -e "\033[31mL'installation de Terraform a échoué.\033[0m"
        exit 1
    fi
}

# Fonction pour installer Ansible s'il est absent
install_ansible() {
    echo "Ansible n'estconsole.log(); pas installé. Installation en cours..."
    sudo apt update
    sudo apt install ansible
    if [ $? -eq 0 ]; then
        echo -e "\033[32mAnsible a été installé avec succès.\033[0m"
    else
        echo -e "\033L'installation d'Ansible a échoué.\033[0m"
        exit 1
    fi
}
#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 1/7: INSTALLATION DE TERRAFORM ET ANSIBLE------ #
echo -e "\033[1;35m- Etape 1/7: Installation de terraform et ansible\033[0m"

# Vérification et installation de Terraform si nécessaire
if ! command -v terraform &>/dev/null; then
    install_terraform
else
    echo -e "\033[32mTerraform est déjà installé.\033[0m"
fi

# Vérification et installation de Ansible si nécessaire
if ! command -v ansible &>/dev/null; then
    install_ansible
else
    echo -e "\033[32mAnsible est déjà installé.\033[0m"
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 2/7: VERIFICATION PRESENCE FICHIERS TERRAFORM ------ #
echo -e "\033[1;35m- Etape 2/7: Vérification présence fichiers terraform\033[0m"

# Vérification de la présence du dossier .terraform et du fichier .terraform.lock.hcl
if [ ! -d "$terraform_dir/.terraform" ] || [ ! -f "$terraform_dir/.terraform.lock.hcl" ]; then
    echo -e "\033[33mDossier .terraform ou fichier .terraform.lock.hcl introuvables. Exécution de 'terraform init'...\033[0m"
    cd $terraform_dir
    terraform init
else
    echo -e "\033[32mDossier .terraform et fichier .terraform.lock.hcl déjà présents. Pas besoin d'exécuter 'terraform init'.\033[0m"
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 3/7: TERRAFORM APPLY ------ #
echo -e "\033[1;35m- Etape 3/7: Terraform apply\033[0m"
cd "$terraform_dir"

# Exécuter la commande terraform plan et stocker la sortie dans une variable
plan_output=$(terraform plan)

# Vérifier s'il y a des changements dans la sortie du plan
if echo "$plan_output" | grep -q "No changes"; then
    echo -e "\033[32mAucun changement détecté. Pas besoin de lancer terraform apply.\033[0m"
else
    echo -e "\033[33mDes changements ont été détectés. Terraform apply en cours...\033[0m"
    terraform apply -auto-approve

    echo -e "\033[33mSuppression des anciens hosts car terraform apply a été executé\033[0m"

    cd $terraform_dir
    wordpress_ip=$(terraform output wordpress_instance_ip | sed 's/"//g')
    database_ip=$(terraform output database_instance_ip | sed 's/"//g')

    # Supprimer l'entrée de la clé d'hôte afin d'éviter les erreurs de vérification
    ssh-keygen -R $wordpress_ip
    ssh-keygen -R $database_ip

    # Pause de 30 secondes entre les étapes 5 et 6
    sleep 30
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 4/7: VERIFICATION DE LA PRESENCE DES FICHIERS ANSIBLE ------ #
echo -e "\033[1;35m- Etape 4/7: Vérification de la présence des fichiers Ansible\033[0m"

if [ ! -f "$ansible_dir/inventory.ini" ]; then
    echo -e "\033[33mFichier ansible inventory introuvable. Création du fichier 'inventory.ini'...\033[0m"

    # Génération de l'inventaire avec les adresses IP
    cd $root_dir
    ./generate_inventory.sh >ansible/inventory.ini
else
    echo -e "\033[32mFichiers Ansible déjà présents. Suppression et récréation de l'inventaire\033[0m"

    # Génération de l'inventaire avec les adresses IP
    cd $ansible_dir
    rm -f inventory.ini
    cd $root_dir
    ./generate_inventory.sh >ansible/inventory.ini
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 5/7: RECUPERATIONS CERTAINES VARIABLES NECESSAIRES ------ #
echo -e "\033[1;35m- Etape 5/7: Récupérations de certaines variables nécessaires\033[0m"

cd $terraform_dir
wordpress_internal_ip=$(terraform output wordpress_instance_internal_ip | sed 's/"//g')
database_internal_ip=$(terraform output database_instance_internal_ip | sed 's/"//g')

# Récupération de l'adresse IP interne de wordpress depuis Terraform
# Vérification si la variable existe dans le fichier database/defaults/main.yml
if grep -q 'wordpress_instance_internal_ip' $ansible_dir/roles/database/defaults/main.yml; then
    current_ip=$(grep 'wordpress_instance_internal_ip' $ansible_dir/roles/database/defaults/main.yml | cut -d'"' -f2)
    # Comparaison avec la nouvelle IP
    if [ -z "$current_ip" ]; then
        echo "L'adresse IP actuelle est vide. Mise à jour avec la nouvelle valeur."
        sed -i "s/^wordpress_instance_internal_ip:.*/wordpress_instance_internal_ip: \"$wordpress_internal_ip\"/" $ansible_dir/roles/database/defaults/main.yml
    elif [ "$current_ip" != "$wordpress_internal_ip" ]; then
        sed -i "s/$current_ip/$wordpress_internal_ip/" $ansible_dir/roles/database/defaults/main.yml
        echo "La valeur de wordpress_instance_internal_ip a été mise à jour dans le fichier main.yml."
    else
        echo "La valeur de wordpress_instance_internal_ip est déjà à jour dans le fichier main.yml."
    fi
else
    # Création du contenu à ajouter au fichier main.yml
    new_content="# Variable wordpress internal IP - Cette variable est construite automatiquement après le lancement du script ./deployement.sh. Ne pas changer. \nwordpress_instance_internal_ip: \"$wordpress_internal_ip\""

    # Ajout du contenu à la fin du fichier main.yml
    echo -e $new_content >>$ansible_dir/roles/database/defaults/main.yml
fi

# Récupération de l'adresse IP interne de database depuis Terraform
# Vérification si la variable existe dans le fichier main.yml
if grep -q 'database_instance_internal_ip' $ansible_dir/roles/wordpress/defaults/main.yml; then
    current_ip=$(grep 'database_instance_internal_ip' $ansible_dir/roles/wordpress/defaults/main.yml | cut -d'"' -f2)
    # Comparaison avec la nouvelle IP
    if [ -z "$current_ip" ]; then
        echo "L'adresse IP actuelle pour database_instance_internal_ip est vide. Mise à jour avec la nouvelle valeur."
        sed -i "s/^database_instance_internal_ip:.*/database_instance_internal_ip: \"$database_internal_ip\"/" $ansible_dir/roles/wordpress/defaults/main.yml
    elif [ "$current_ip" != "$database_internal_ip" ]; then
        sed -i "s/$current_ip/$database_internal_ip/" $ansible_dir/roles/wordpress/defaults/main.yml
        echo "La valeur de database_instance_internal_ip a été mise à jour dans le fichier main.yml."
    else
        echo "La valeur de database_instance_internal_ip est déjà à jour dans le fichier main.yml."
    fi
else
    # Création du contenu à ajouter au fichier main.yml
    new_content="# Variable database internal IP - Cette variable est construite automatiquement après le lancement du script ./deployement.sh. Ne pas changer. \ndatabase_instance_internal_ip: \"$database_internal_ip\""

    # Ajout du contenu à la fin du fichier main.yml
    echo -e $new_content >>$ansible_dir/roles/wordpress/defaults/main.yml
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 6/7: LANCEMENT PLAYBOOKS ANSIBLE ------ #
echo -e "\033[1;35m- Etape 6/7: Lancement playbooks ansible\033[0m"

cd $ansible_dir
ansible-playbook -i inventory.ini -b playbooks/database.yml
ansible-playbook -i inventory.ini -b playbooks/wordpress.yml

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 7/7: VERIFICATION FONCTIONNEMENT APPLICATION ------ #
echo -e "\033[1;35m- Etape 7/7: Vérification fonctionnement application\033[0m"

cd $terraform_dir
wordpress_ip=$(terraform output wordpress_instance_ip | sed 's/"//g')
curl_output=$(curl -s $wordpress_ip/wordpress)

echo $wordpress_ip
echo $curl_output
if echo "$curl_output" | grep -o "html"; then
    echo "L'application WordPress est fonctionnelle."

    # Récupération du titre de la page WordPress pour l'élément visuel
    title=$(echo "$curl_output" | grep -o "<title>[^<]*" | sed -e 's/<title>//')
    echo "Titre de la page WordPress : $title"
else
    echo "L'application WordPress ne semble pas fonctionner correctement."
fi
