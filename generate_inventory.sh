# Chemins
terraform_dir="$root_dir/terraform/configuration"

cd $terraform_dir
wordpress_ip=$(terraform output wordpress_instance_ip | sed 's/"//g')
database_ip=$(terraform output database_instance_ip | sed 's/"//g')
user=$(terraform output instance_user | sed 's/"//g')

# Génération de l'inventaire avec les adresses IP
echo "[wordpress]"
echo $wordpress_ip ansible_user=$user

echo "[database]"
echo $database_ip ansible_user=$user
