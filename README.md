# TP1 GCP TERRAFORM ANSIBLE

## Dossier du projet

La racine de mon projet contient les éléments suivants:

- README.md
- **deployment.sh**
- **terraform** (dossier)
- **ansible** (dossier)
- generate_inventory.sh
- terraform_destroy.sh

## Prérequis

- 1 ordi : ordi portable ou ordi fixe
- 1 ligne de commande linux : soit vm ubuntu soit wsl sur windows
- 1 compte GCP avec un projet
- 1 crédentials.json
- 1 clé ssh
- Modification de 3 variables
- 1 petit peu de patience, mais pas beaucoup

### Création du credentials.json ou clé de compte de service

Dans GCP - https://console.cloud.google.com/ - aller sur :

- 1 - IAM et administration
- 2 - Compte de service
- 3 - Créez un compte de service
- 4 - Entrez un nom puis continuez
- 5 - Séléctionnez le rôle propriétaire (ou éditeur) puis finalisez la création
- 6 - Cliquez sur le compte crée
- 7 - Cliquez sur clé en haut
- 8 - Cliquer sur ajouter une clé, en json
- 9 - Télécharger la clé en le renommant credentials.json
- 10 - Mettez le dans le dossier terraform/private (le dossier private n'existe pas créez le)

### Modif variables - IMPORTANT

1 - Dans **terraform/configuration/terraform.tfvars** :

    `project_id = "mettez_votre_project_id"` 

*trouvable dans le fichier credentials.json*

2 - Dans **terraform/configuration/variables.tf** :

    `variable user => default = "mettez_votre_nom_user_gcp"` 

*début de votre mail google (c'est pour la clé ssh)*

3 - Dans **deployment.sh** :

    `root_dir="mettre_le_chemin_absolu_du_projet"` 

*c'est le dossier du projet à la racine*

## Script de déploiement : deployment.sh

Ce script est l'élément principal qui fait toutes les actions permettant la création et l'activation de wordpress. Il y au total 7 étapes qui ont amené au bout le projet.

#### ETAPE 1/7: INSTALLATION DE TERRAFORM ET ANSIBLE

Cette étape permet d'installer terraform et ansible sur votre machine linux si ces deux logiciels ne sont pas présent. Elle utilise les deux fonctions préalablement crée.

#### ETAPE 2/7: VERIFICATION PRESENCE FICHIERS TERRAFORM

Cette étape vérifie si les dossiers et fichiers du terraform init (.terraform et .terraform.lock.hcl) sont présents dans l'emplacement de terraform. Dans le cas ou ces élements ne sont pas présents **terraform init** se lance.

#### ETAPE 3/7: TERRAFORM APPLY

Cette étape effectue un **terraform plan** et vérifie si il y a eu des changements. Dans le cas où il y a eu des changements **terraform apply** est lancé. C'est à partir d'ici que les instances sur GCP sont crées.

Dans le cas où **terraform apply** est executé à la fin de cette étape je supprime également les hosts qui pourraient être resté dans le fichier known_hosts. En effet cela pourrait engender un message de sécurité empechant la connexion en ssh sur les serveurs lors de l'étape du lancement des playbooks. Un délai après le apply d'environ 30 secondes a été également ajouter afin d'attendre la création complète des instances avant d'essayer de s'y connecter.

#### ETAPE 4/7: VERIFICATION DE LA PRESENCE DES FICHIERS ANSIBLE

Cette étape vérifie si le fichier inventory.ini est présent dans le dossier ansible. Si non l'inventory est crée via le script **generate_inventory.sh**. Le script utilise le renvoi des variables via un **terraform outpout** et les utilise afin de dynamiser l'inventory. Les élements dans le script ne sont que des echos qui vienne se rajouter dans le fichier inventory.

#### ETAPE 5/7: RECUPERATIONS CERTAINES VARIABLES NECESSAIRES

Certaines variables de terraform sont nécéssaires dans le script mais également dans les tâches ansibles. Dans cette étape on récupère ces variables et on vient les rajouter à la fin des fichier defaults/main.yml de wordpress et ou database. 

En effet par exemple **l'adresse ip interne du serveur wordpress** pourrait être nécéssaire au serveur database afin de créer un user.

Et l'adresse ip interne du serveur database est nécéssaire pour wordpress afin de créer la connexion de wordpress au database par exemple via le fichier **wp-config.php**

#### ETAPE 6/7: LANCEMENT PLAYBOOKS ANSIBLE

Cette étape permet de lancer les playbooks ansible de database et de wordpress en utilisant le fichier inventory précédemenrt crée.

#### ETAPE 7/7: VERIFICATION FONCTIONNEMENT APPLICATION

Cette étape permet de vérifier qu'un curl sur le serveur wordpress renvoie bien un code html en rapport avec wordpress.

## Dossier terraform

Dans ce dossier se trouve les dossiers et fichiers qui me permettent de créer les vm, le réseau, le sous-réseau, les firewalls. Certaines variables ont été envoyés via les outpouts pour y avoir accés en ligne de commande pour le script ainsi que pour les tâches ansibles.

### Composition
Dans le dossier terraform se trouve **un dossier de configuration** où se trouve tous les fichiers terraform .tf mais également **un dossier private** qui n'est pas crée de base (à vous de le créer) et où se trouve le fichier **credentials.json**.

### Credentials.json
Attention le fichier **credentials.json** étant un fichier contenant des informations sensibles, il ne faut pas le push sur github. Ce fichier contenant les informations de connexion de votre GCP, il est nécéssaire que vous le mettiez en en place et que vous le mettiez dans le dossier private à cet emplacement.

### Modules
Les modules sont utilisés dans le dossier de configuration afin de faciliter la maintnabilité du code et pour bien scinder les éléments. L'utilisation des modules incite à utiliser les outpouts afin d'envoyer des informations d'une module à une autre.

### Pare feu

Pour que les connexion marchent comme il faut, il faut, en plus de la création de réseau et sous réseau, la création de 3 régle de pare feu.

- port 80 et 443 sur le serveur wordpress pour permettre le http et https
- port 22 sur les deux serveurs pour la connexion ssh
- port 3306 sur les deux serveurs pour permettre la connexion à la base de donnée

### Variables
Deux fichier variables sont présents :
- terraform.tvars contenant le nom de votre votre projet que vous devez mettre et l'emplacement du credential file
- variables.tf contenant une variable user à modifier par l'user de votre GCP afin d'envoyer la clé ssh avec le bon user.

## Dossier ansible

Dans ce dossier j'utilise 2 playbooks un pour le **database** et un pour le **wordpress** ainsi que leurs rôles respectives avec la structure d'ansible-galaxy.

### Composition
Le dossier ansible est constitué de 4 éléments : 

- playbooks (dossier)
- roles (dossier)
- ansible.cfg
- inventory.ini (ce fichier est crée avec le script de déploiement)

### Rôle database

Possède 2 fichier yml dans les tasks qui permettent : 
- l'installation des paquets prérequis 
- le changement du mot de passe root pour la première fois
- la création du database wordpress dans mysql 
- la création d'un user avec un accès depuis le serveur wordpress
- la mise à jour du fichier mysqld.cnf afin de permettre l'accès depuis les autres adresses ip comme celui du serveur wordpress. Ceci n'est pas dangereux car le pare feu à bien été configuré depuis terraform afin de ne permettre que au serveur wordpress d'y avoir accés. 

### Rôle wordpress

Possède 2 fichier yml dans les tasks qui permettent :

- l'installation des paquets prérequis
- l'activation de mysqli dans php
- téléchargement de wordpress
- suppression fichier apache par défaut
- renommage et configuration fichier wp-config.php avec un template jinjia présent dans le dossier template

### Fichier ansible.cfg

Contient des valeurs par défaut s'appliquant aux playbooks d'ansibles et aux rôles. Configurable.

### Fichier inventory.ini
Ce fichier n'est pas présent au début, le fichier est crée via le script de déploiement.*

Possède les informations sur les hosts ainsi que le compte permettant de de s'y connecter en ssh..

## Script : generate_inventory.sh

Permet la création du fichier d'inventaire d'ansible, néanmoins ce script ne se lance pas directement, mais fait parti intégrante d'un étape du script principal.

## Script : terraform_destroy.sh

Permet la suppression de tout les éléments sur GCP en rapport avec le projet. Ce script facilite la suppression car se trouvant à la racine du projet.

## Arborescense du projet

```bash
  .
├── README.md
├── ansible
│   ├── ansible.cfg
│   ├── inventory.ini
│   ├── playbooks
│   │   ├── database.yml
│   │   └── wordpress.yml
│   └── roles
│       ├── database
│       │   ├── README.md
│       │   ├── defaults
│       │   │   └── main.yml
│       │   ├── handlers
│       │   │   └── main.yml
│       │   └── tasks
│       │       ├── install_packages_database.yml
│       │       └── main.yml
│       └── wordpress
│           ├── README.md
│           ├── defaults
│           │   └── main.yml
│           ├── handlers
│           │   └── main.yml
│           ├── tasks
│           │   ├── install_packages_wordpress.yml
│           │   └── main.yml
│           └── templates
│               └── wp-config.php.j2
├── deployment.sh
├── generate_inventory.sh
├── terraform
│   ├── configuration
│   │   ├── database-vm-module
│   │   │   ├── main.tf
│   │   │   └── variables.tf
│   │   ├── main.tf
│   │   ├── network-module
│   │   │   ├── main.tf
│   │   │   └── variables.tf
│   │   ├── output.tf
│   │   ├── terraform.tfstate
│   │   ├── terraform.tfstate.backup
│   │   ├── terraform.tfvars
│   │   ├── variables.tf
│   │   └── wordpress-vm-module
│   │       ├── main.tf
│   │       └── variables.tf
│   └── private
│       └── credentials.json
└── terraform_destroy.sh
```

Fin du readme, je vous remercie