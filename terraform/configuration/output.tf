output "instance_user" {
  value = var.user
}

output "wordpress_instance_internal_ip" {
  value = module.wordpress_vm.wordpress_instance_internal_ip
}

output "wordpress_instance_ip" {
  value = module.wordpress_vm.wordpress_instance_ip
}

output "database_instance_internal_ip" {
  value = module.database_vm.database_instance_internal_ip
}

output "database_instance_ip" {
  value = module.database_vm.database_instance_ip
}
