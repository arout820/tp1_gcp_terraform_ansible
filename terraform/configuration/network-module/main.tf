#réseau
resource "google_compute_network" "tp1_network" {
  name                    = "tp1-network"
  auto_create_subnetworks = false
}

# sous-réseau pour wordpress et database
resource "google_compute_subnetwork" "tp1_subnetwork" {
  name          = "tp1-subnetwork"
  ip_cidr_range = "10.0.0.0/24"
  region        = "us-east1"
  network       = google_compute_network.tp1_network.id
}

# firewall ssh
resource "google_compute_firewall" "firewall_ssh" {
  name    = "allow-ssh"
  network = google_compute_network.tp1_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}

# firewall http https
resource "google_compute_firewall" "firewall_http" {
  name    = "allow-http-https"
  network = google_compute_network.tp1_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
}

# firewall database
resource "google_compute_firewall" "firewall_database" {
  name    = "allow-database"
  network = google_compute_network.tp1_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["3306"]
  }

  source_ranges = ["10.0.0.0/24"]
  target_tags   = ["db-server", "http-server"]
}

output "network_id" {
  value = google_compute_network.tp1_network.id
}

output "tp1_subnetwork_id" {
  value = google_compute_subnetwork.tp1_subnetwork.id
}

