resource "google_compute_instance" "database_instance" {
  name         = "database-instance"
  machine_type = "g1-small"
  zone         = "us-east1-b"
  tags         = ["db-server"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  network_interface {
    network    = var.network_id
    subnetwork = var.tp1_subnetwork_id
    access_config {
      // necessary even empty
    }
  }
}

output "database_instance_internal_ip" {
  value = google_compute_instance.database_instance.network_interface[0].network_ip
}

output "database_instance_ip" {
  value = google_compute_instance.database_instance.network_interface[0].access_config[0].nat_ip
}
