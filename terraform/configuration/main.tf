# PROVIDER
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project_id
  region      = var.region
}

# MODULES
module "network" {
  source = "./network-module"
}

module "wordpress_vm" {
  source            = "./wordpress-vm-module"
  network_id        = module.network.network_id
  tp1_subnetwork_id = module.network.tp1_subnetwork_id
}

module "database_vm" {
  source            = "./database-vm-module"
  network_id        = module.network.network_id
  tp1_subnetwork_id = module.network.tp1_subnetwork_id
}

# CLE SSH
resource "google_compute_project_metadata_item" "ssh_keys" {
  key   = "ssh-keys"
  value = "${var.user}:${file("~/.ssh/id_rsa.pub")}"
}
